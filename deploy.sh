#!/bin/bash

# Defining the environmental variable file
#set nexus image
NEXUS_VERSION='sonatype/nexus3'

#set container name for nexus
NEXUS_CONTAINER_NAME='nexus'

#set nexus repository 
NEXUS_HOME='/nexus-data'
 
#set nexus port
NEXUS_PORT='8081:8081'

#set alfresco image
ALFRESCO_IMAGE='alfresco/alfresco-content-repository-community:6.1.2-ga'

#set alfresco environment
REPO_HOST='alfresco'
REPO_PORT='8080'

#set share image
ALFRESCO_SHARE_IMAGE='alfresco/alfresco-share:6.1.0-RC3'

#set postgres for alfresco 
POSTGRES_ALFRESCO_PASSWORD='5E4bJhHz%.G8^J'
POSTGRES_ALFRESCO_USER='alfresco'
POSTGRES_ALFRESCO_DB='alfresco'

#set alfresco datas
ALFRESCO_DATA='/usr/local/tomcat/alf_data'
ALFRESCO_LOGS='/usr/local/tomcat/logs'

#set postgres alfresco data
POSTGRES_ALFRESCO_DATA='/var/lib/postgresql/data'
POSTGRES_ALFRESCO='/var/log/postgresql'

#set solr6 image
SOLAR_IMAGE='alfresco/alfresco-search-services:1.3.0-RC2'

#set solar environment
SOLR_ALFRESCO_HOST='alfresco'
SOLR_ALFRESCO_PORT='8080'
SOLR_SOLR_HOST='solr6'
SOLR_SOLR_PORT='8983'
SOLR_CREATE_ALFRESCO_DEFAULTS='alfresco,archive'

#set solar data
SOLAR_DATA='/opt/alfresco-search-services/data'

#set activemq alfresco
ACTIVEMQ_IMAGE='alfresco/alfresco-activemq:5.15.6'
read -p "Start config system....Press [Enter] key to start continue..."
sudo yum install wget -y >> /dev/null 2>&1 &
sudo yum install git -y >> /dev/null 2>&1 &

#Intall requirements for the server 
#configure server env
read -p "Start changing Server Name ....Press [Enter] key to start continue..."
sudo hostnamectl set-hostname devops
sudo hostname -s 
#create file requirements.sh 
#install ansible
read -p "Start installing Ansible.....Press [Enter] key to start continue..."
sudo yum makecache 
sudo yum install dnf -y 
sudo yum install ansible -y
#install jenkins
read -p "Start installing Jenkins ....Press [Enter] key to start continue..."
sudo yum install java-11-openjdk-devel-11.0.13.0.8-1.el7_9 -y 
sudo yum install java-1.8.0-openjdk-devel -y
java -version
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo 
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key 
sudo yum install jenkins -y 
sudo systemctl start jenkins 
sudo systemctl status jenkins 
#configure firewall
sudo yum install firewalld -y 
sudo systemctl enable firewalld --now 
sudo yum install -y yum-utils device-mapper-persistent-data lvm2 
#configure ports
read -p "Start enabling ports....Press [Enter] key to start continue..."

sudo firewall-cmd --zone=public --add-masquerade --permanent 
sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=8081/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=50000/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=8983/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=8085/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=8086/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=9001/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=5432/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=8161/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=5672/tcp
sudo firewall-cmd --zone=public --permanent --add-port=61616/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=61613/tcp 
sudo  firewall-cmd --reload 
sudo sysctl -w vm.max_map_count=524288 
sudo sysctl -w fs.file-max=131072 
echo "end with install all requirements...."
read -p "Press [Enter] key to start continue..."
echo "51.38.224.13 devops" | sudo tee /etc/hosts

#create mount directories for nexus,sonarQube,postgres and jenkins
echo "Starting create mount directories for nexus,sonarQube,postgres and jenkins ...."
mkdir ~/data
mkdir ~/data/logs

mkdir ~/data/jenkins_data
mkdir ~/data/nexus-data
mkdir ~/data/alf-repo-data
mkdir ~/data/postgres-alfresco-data
mkdir ~/data/solr-data
mkdir ~/data/logs/alfresco
mkdir ~/data/logs/postgres-alfresco


cd data 

sudo chmod -R 755 nexus-data
sudo chmod -R 755 *_*
sudo chmod -R 755 postgres-alfresco-data
sudo chown -R 755 postgres-alfresco-data
sudo chown -R 200 nexus-data
sudo chown -R 1000  *_*
sudo chown -R 33007 solr-data
sudo chown -R 33000 alf-repo-data
cd logs
sudo chown -R 33000 alfresco
sudo chown -R 755 postgres-alfresco

cd ../..
echo "create a new file docker-compose.yml ...."
sudo useradd jenkins

echo "Starting create user jenkins ...."
##create a new file docker-compose.yml to install nexus,soner et postgres 
#
sudo echo -e " #create docker compose file to install servers 
version: '3.8'
services:

  nexus:
    image: ${NEXUS_VERSION}
    restart: on-failure
    container_name: ${NEXUS_CONTAINER_NAME}
    ports:
      - ${NEXUS_PORT}
    volumes:
      - ~/data/nexus-data:${NEXUS_HOME}
    environment:
      - Xms2703m 
      - Xmx2703m 
      - XX:MaxDirectMemorySize=2703m
    ulimits:
      nofile:
        soft: 65536
        hard: 65536
      " > docker-compose.yml
echo "File docker-compose.yml was created with SUCCESS ...."
#
echo "Start create a docker compose file to install alfresco  ...."
#create a docker compose file to install alfresco 
sudo echo -e "# This docker-compose file will spin up an ACS cluster on a local host or on a server and it requires a minimum of 12GB Memory to distribute among containers.
# Limit container memory and assign X percentage to JVM.  There are couple of ways to allocate JVM Memory for ACS Containers
# For example: 'JAVA_OPTS: $JAVA_OPTS -XX:+PrintFlagsFinal -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap
# But, as per Oracle docs (https://docs.oracle.com/javase/9/gctuning/parallel-collector1.htm#JSGCT-GUID-CAB83393-3438-44ED-98F0-D15641B43C7D)
# If container memory is not explicitly set, then the above flags will default max heap to 1/4th of container's memory which may not be ideal.
# Hence, setting up explicit Container memory and then assigning a percentage of it to the JVM for performance tuning.

# Using version 2 as 3 does not support resource constraint options (cpu_*, mem_* limits) for non swarm mode in Compose
version: "'"2"'"

services:
    alfresco:
        image: ${ALFRESCO_IMAGE}
        mem_limit: 1500m
        environment:
            JAVA_OPTS : "'"
                -Ddb.driver=org.postgresql.Driver
                -Ddb.username=alfresco
                -Ddb.password=5E4bJhHz%.G8^J
                -Ddb.url=jdbc:postgresql://postgres:5432/alfresco
                -Dsolr.host=solr6
                -Dsolr.port=8983
                -Dsolr.secureComms=none
                -Dsolr.base.url=/solr
                -Dindex.subsystem.name=solr6
                -Dshare.host=localhost
                -Dalfresco.port=8085
                -Daos.baseUrlOverwrite=http://localhost:8085/alfresco/aos
                -Dmessaging.broker.url=\"failover:(nio://activemq:61616)?timeout=3000&jms.useCompression=true\"
                -Ddeployment.method=DOCKER_COMPOSE
                -Dcsrf.filter.enabled=false
                -Xms1g -Xmx1g
                "'"
                
        ports:
            - 8085:8080 #Browser port
        volumes:
            - ~/data/alf-repo-data:${ALFRESCO_DATA}
            - ~/data/logs/alfresco:${ALFRESCO_LOGS}
        restart: always
    share:
        image: ${ALFRESCO_SHARE_IMAGE}
        mem_limit: 1g
        environment:
            - REPO_HOST=${REPO_HOST}
            - REPO_PORT=${REPO_PORT}
            - "'"CATALINA_OPTS= -Xms500m -Xmx500m"'"
        ports:
            - 9001:8080
        restart: always
    postgres:
        image: postgres:10.1
        mem_limit: 1500m
        environment:
            - POSTGRES_PASSWORD=${POSTGRES_ALFRESCO_PASSWORD}
            - POSTGRES_USER=${POSTGRES_ALFRESCO_USER}
            - POSTGRES_DB=${POSTGRES_ALFRESCO_DB}
        command: postgres -c max_connections=300 -c log_min_messages=LOG
        ports:
            - 5432:5432
        volumes:
            - ~/data/postgres-alfresco-data:${POSTGRES_ALFRESCO_DATA}
            - ~/data/logs/postgres-alfresco:${POSTGRES_ALFRESCO}
        restart: always
    solr6:
        image: ${SOLAR_IMAGE}
        mem_limit: 2500m
        environment:
            #Solr needs to know how to register itself with Alfresco
            - SOLR_ALFRESCO_HOST=${SOLR_ALFRESCO_HOST}
            - SOLR_ALFRESCO_PORT=${SOLR_ALFRESCO_PORT}
            #Alfresco needs to know how to call solr
            - SOLR_SOLR_HOST=${SOLR_SOLR_HOST}
            - SOLR_SOLR_PORT=${SOLR_SOLR_PORT}
            #Create the default alfresco and archive cores
            - SOLR_CREATE_ALFRESCO_DEFAULTS=${SOLR_CREATE_ALFRESCO_DEFAULTS}
            - "'"SOLR_JAVA_MEM=-Xms2g -Xmx2g"'"
        ports:
            - 8086:8983 #Browser port
        volumes:
            - ~/data/solr-data:${SOLAR_DATA}
        restart: always
    activemq:
        image: ${ACTIVEMQ_IMAGE}
        mem_limit: 2048m
        ports:
            - 8161:8161 # Web Console
            - 5672:5672 # AMQP
            - 61616:61616 # OpenWire
            - 61613:61613 # STOMP
        restart: always

       " > docker-compose-alfresco.yml
echo "File docker-compose-alfresco.yml was created with SUCCESS ...."
#####
echo "Start Installing docker package ...."
#Install docker package
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y  docker-ce-20.10.9-3.el7  docker-ce-cli-20.10.9-3.el7 containerd.io 
read -p "Start docker [Enter] key to start continue..."
sudo systemctl start docker 
echo "Docker package was installed...."
#configure users
echo "Configure users Docker...."
sudo usermod -aG docker $USER
sudo gpasswd -a $USER docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose 
sudo systemctl daemon-reload
sudo systemctl enable docker
sudo systemctl restart docker
sudo chmod -R 755 docker-compose.yml
sudo chmod -R 755 docker-compose-alfresco.yml
cd 
echo "Add access control to Docker Directories...."
sudo chmod -R 666 /var/run/docker.sock
sudo chown -R 200 /var/run/docker.sock
sudo chmod 755 /home/centos/data/postgres-alfresco-data
sudo chmod 755 /home/centos/data/alf-repo-data
sudo chmod 755 /home/centos/data/solr-data
cd $HOME 
echo "Start jenkins ...."
sudo systemctl start jenkins     
echo "Run docker Compose files ...."
read -p "Press [Enter] key to start continue..."
docker-compose -f docker-compose.yml -f docker-compose-alfresco.yml up -d
echo "show all dockers created ...."
docker ps -a

exit
