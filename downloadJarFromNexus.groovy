#!/usr/bin/env groovy

String message = ""
pipeline {
  agent any
  stages {

    stage('build') {
        steps {
            
            sh 'rm -rf ${artifact}-${version}.${package}' 
            sh 'curl -o ${artifact}-${version}.${package} -L -u aymax-devops:admin123  -X GET -k "http://51.38.224.13:8081/service/rest/v1/search/assets/download?sort=version&repository=${repository}&group=${group}&maven.artifactId=${artifact}&maven.baseVersion=${version}&maven.extension=${package}" -H "accept: application/json" '
            
    
        }
    }

    stage('SSH transfer') {
     steps {
      script {
        sshPublisher(
          continueOnError: false, failOnError: true,
          publishers: [
            sshPublisherDesc(
              configName: "${server_name}",
              verbose: true,
              usePromotionTimestamp: false,
              useWorkspaceInPromotion: false,
              transfers: [
                sshTransfer(
                  cleanRemote: true,
                  excludes: '',
                  execCommand: '',
                  execTimeout: 300000,
                  flatten: false,
                  makeEmptyDirs: false,
                  noDefaultExcludes: false,
                  patternSeparator: '[, ]+',
                  remoteDirectory: "${app_name}/target",
                  remoteDirectorySDF: false,
                  removePrefix: '',
                  sourceFiles: "**/*"
                )
              ])
          ])
      }
      }
    }

     stage('deploy artefact to target server') {
      steps {

       
        sh 'set +x && cd "/etc/ansible/helssy/" &&  ansible-playbook "/etc/ansible/helssy/"/backend-deploy.yml --user=root -e "server_name="${server_name}"  db_name="${server_name}" application_name="${app_name}" IMAGE_TAG=1.0.'+"${env.BUILD_ID}"+'-'+"${env.GIT_COMMIT}"+'-"'
       
      }
    }
  }
}